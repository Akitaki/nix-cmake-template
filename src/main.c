#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
  int nums[4] = {0};

  for (int i = 0; i < 4; ++i) {
    int ch = getchar();
    assert(ch != EOF);
    nums[i] = (ch - '0' + 7) % 10;
  }

  printf("%d%d%d%d\n", nums[2], nums[3], nums[0], nums[1]);
}
